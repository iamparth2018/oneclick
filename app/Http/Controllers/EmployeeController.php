<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Skill;

class EmployeeController extends Controller
{
    public function employee(){

    	$employee = Employee::all();
    	//dd($data);
        
        return view('employee',compact('employee'));
    }

    public function addemployee(Request $request){

    	
    	$skills = explode(',', $request->emp_skills);
    	
    	foreach ($skills as $key => $value) {
    		
    		$checkskills = Skill::where('skill_name','=',$value)->first();
    		
    		if(empty($checkskills)){
    			$skills=Skill::create([
    				'skill_name' => $value,
    			]);
    		}
    	}

    	$employee = Employee::create([
                'name' => $request->emp_name,
                'skills' => $request->emp_skills,
            ]);

    	return redirect('employee')->with('success', 'Employee added successfully');
    }

    
    public function editemployee(Request $request){
    	//dd($request);
    	$findEmp = Employee::find($request->emp_id);

    	if($findEmp != ''){
        
	        $skills = explode(',', $request->e_skills);
	    	
	    	foreach ($skills as $key => $value) {
	    		
	    		$checkskills = Skill::where('skill_name','=',$value)->first();
	    		
	    		if(empty($checkskills)){
	    			$skills=Skill::create([
	    				'skill_name' => $value,
	    			]);
	    		}
	    	}


            $findEmp->name = $request->e_name;
            $findEmp->skills = $request->e_skills;
            $findEmp->save();

            return redirect('employee')->with('success', 'Employee Update successfully');;
        }
    }


    public function delemployee(Request $request){

    	$employee = Employee::find($request->emp_id);
    	$employee->delete();

    	return redirect('employee')->with('success', 'Employee Deleted successfully');
    }
}
