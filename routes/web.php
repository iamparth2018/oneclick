<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/employee', 'EmployeeController@employee');
Route::post('/addemployee','EmployeeController@addemployee')->name('addemployee');
Route::post('/editemployee','EmployeeController@editemployee')->name('editemployee');
Route::post('/delemployee','EmployeeController@delemployee')->name('delemployee');
