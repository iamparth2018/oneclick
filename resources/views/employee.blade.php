<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Employee List</h2>

 <div class="col-md-12">
    <div class="col-md-8">
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#add_emp_Modal">Add Employee</button>
    </div>
    <div class="col-md-4">
        <form action="">
            <div class="form-row">
                <div class="col-md">
                    <input type="text" name="search" class="form-control" placeholder="search By Name" value="">
                </div>
                <div class="">
                    <button type="submit" class="btn btn-info btn-block">Search</button>
                </div>
            </div>
        </form>  
    </div>
</div>



  <!-- Add Employee Modal -->
  <div class="modal fade" id="add_emp_Modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Employee</h4>
        </div>
        <div class="modal-body">
        	<form action="{{ route('addemployee') }}" method="POST" name="addemployeeData">
                        @csrf

		        <div class="col-md-12">
		        	<label> Employee Name</label>
		        	<input type="text" name="emp_name" id="emp_name" required="" class="form-control">
		    	</div>
		    	<div class="col-md-12">
		        	<label>Employee Skills</label>
		        	<input type="text" name="emp_skills" id="emp_skills" required="" class="form-control">
		        </div>

		        <div class="form-group col-xs-12 col-md-12">
                    <button type="submit" class="btn btn-primary">Save</button>
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
		    </form>
        </div>
        <div class="modal-footer">
         
        </div>
      </div>
      
    </div>
  </div>

    <!--Edit Employee Modal -->
  	<div class="modal fade" id="edit_emp_Modal" role="dialog">
	    <div class="modal-dialog">
	    
	      <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Edit Employee</h4>
	        </div>
	        <div class="modal-body">
	        	<form action="{{ route('editemployee') }}" method="POST" name="addemployeeData">
	                        @csrf
	                        <input type="hidden" name="emp_id" id="emp_id">
			        <div class="col-md-12">
			        	<label> Employee Name</label>
			        	<input type="text" name="e_name" id="e_name" required="" class="form-control">
			    	</div>
			    	<div class="col-md-12">
			        	<label>Employee Skills</label>
			        	<input type="text" name="e_skills" id="e_skills" required="" class="form-control">
			        </div>

			        <div class="form-group col-xs-12 col-md-12">
	                    <button type="submit" class="btn btn-primary">Save</button>
	                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                </div>
			    </form>
	        </div>
	        <div class="modal-footer">
	         
	        </div>
	      </div>
	      
	    </div>
  	</div>

  	 <!--Delete Employee Modal -->
  	<div class="modal fade" id="del_emp_Modal" role="dialog">
	    <div class="modal-dialog">
	    
	      <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Delete Employee</h4>
	        </div>
	        <div class="modal-body">
	        	<form action="{{ route('delemployee') }}" method="POST" name="delemployeeData">
	                        @csrf

			        <p>Are you Sure You want to delete this Employee ?</p>
			        <input type="hidden" name="emp_id" id="emp_id">
			        <div class="form-group col-xs-12 col-md-12">
	                    <button type="submit" class="btn btn-primary">Yes</button>
	                     <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
	                </div>
			    </form>
	        </div>
	        <div class="modal-footer">
	         
	        </div>
	      </div>
	      
	    </div>
  	</div>
  
 
</div>
<div class="container">&nbsp;</div>


<div class="container">

	 <div class="col-md-12">
  	<ul>
  		@foreach($employee as $value)
  		<li>{{$value->name}} {{$value->skills}} <button type="button" class="btn btn-info edit_employee" data-eid="{{ $value->id }}" data-ename="{{$value->name}}" data-eskill="{{$value->skills}}">Edit</button> <button type="button" class="btn btn-danger delete_employee" data-did="{{ $value->id }}">Delete</button></li><br>
  		@endforeach
  	</ul>
  </div>

</div>

<script>

	$(document).on('click','.edit_employee',function(){
        
        var id = $(this).data('eid');
        var name= $(this).data('ename');
        var skills= $(this).data('eskill');
        $('#emp_id').val(id);
        $('#e_name').val(name);
        $('#e_skills').val(skills);
        $('#edit_emp_Modal').modal('show');      
    });


	$(document).on('click','.delete_employee',function(){
        
        var id = $(this).data('did');
        $('#emp_id').val(id);
        $('#del_emp_Modal').modal('show');      
    });
</script>

</body>
</html>
